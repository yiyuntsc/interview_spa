/**
 * Please feel free to add dummy data here. 
 * Your entire app does not need to persist data in a database, as long as 
 * your changes are valid throughout the app's lifecycle. To make it clear, 
 * it is OK if I refresh the page and lose all the changes, but while I am 
 * still on this page, the CRUD (Create, Read, Update, Delete) functionalities 
 * should work as expected.
 */

var TVShows = [
  {
    id:         1,
    name:       "agents of shield",
    tv_channel: "ABC",
    year:       "2013",
    language:   "English",
    image_url:  "http://photocdn.sohu.com/kis/fengmian/1193/1193727/1193727_ver_big.jpg",
    thumbnail_url:  "http://photocdn.sohu.com/kis/fengmian/1193/1193727/1193727_ver_big.jpg",
    genre:      "action",
    website:    "http://abc.go.com/shows/marvels-agents-of-shield"
  }
];
