# interview_spa 面试问题:单页web app

---

## Task

Make a Single Page Application (SPA) that allows users to build a collection of their favorite TV shows.

## Features
* Users can **add** new show to their collection
* Users can **edit** the show they've added
* Users can **delete** a show from their collection

## Resources
1. A simple design mark up is located in `resources/design/`
2. A basic data model is created for you at `js/main.js`


## Requirements
1. By Single Page Application, it means there can only be **ONE** HTML file **index.html**, all view switching logic has to be done using JavaScript. You can append your new code inside `main.js`.
2. No external UI framework (like Bootstrap, Semantic UI, etc.) should be used
3. Aside from `jQuery`, no other JavaScript libraries should be used
4. Design file is for reference, despite its simple look, you should make your app look as similar to it as possible, please refer to **Technical Specs** below for details
5. In the given files, I have included some **very important** comments, please read them carefully before writing your code.
6. Please include comments wherever you think necessary in your HTML, CSS, JavaScript code

## Technical Specs
You single page application will contain 4 views, a **browsing** view, a **details** view, a **addnew** view and a **edit** view.

* In **ALL** views, your title should be centered

* In **browsing** view (design file: `resources/design/browsing.jpg`):
!["Browsing view mock up"](resources/design/browsing.jpg)
  * The spacing between each image should be the same, and the images as a whole should be centered
  * The label text under each image should be centered to the image
  * When clicking on 'Details', your app should render the **details** view, when clicking on 'Edit', your app should render the **edit** view.
  * When clicking on 'add new show', render **addnew** view

* In **addnew** view (design file: `resources/design/addnew.jpg`):
!["Addnew view mock up"](resources/design/addnew.jpg)
  * You should have an image place holder on the left hand side
  * A form on the right hand side to accept user input
  * When clicking on 'save', save this show by appending its data to the data model defined in `js/main.js`, when clicking on 'cancel', discard changes and render **browsing** view.


* In **details** view (design file: `resources/design/details.jpg`):
!["Details view mock up"](resources/design/details.jpg)
  * You should append the currently selected TV show's name after "Details of"
  * Image of that show is shown on the left hand side, note that the size of the image is **different** from the ones in **browsing** view, you can use a different image here by adding an additional field in data model inside `js/main.js`
  * On the right hand side shows all the info about this TV show, you can use an HTML `<table>` or `<ul>` for this.
  * Below these two elements are two buttons, when clicking on "back to browse", render the **browsing** view, when clicking on "edit", render the **edit** view

* In **edit** view (design file: `resources/design/edit.jpg`):
!["Edit view mock up"](resources/design/edit.jpg)
  * You should append the currently selected TV show's name after "Edit"
  * The right hand side is where you can change the values of this TV show, you start off by rendering their current values inside HTML `<input>` elements
  * When clicking on 'save', update the matching data about this TV show
  * When clicking on 'Cancel', discard changes and render **details** view
  * When clicking on 'Delete', remove this TV show
  * 
  

## Completed
  * Please write here what you have completed, you are more than welcomed to suggest better approaches and design patterns.